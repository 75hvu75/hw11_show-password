'use strict';

// Отримуємо всі елементи з DOM
const passwordInput = document.querySelectorAll('input[type="password"]');
const showPassword = document.querySelectorAll('.icon-password');
const submitButton = document.querySelector('.btn');
const errorMessage = document.createElement('p');

// Обробник подій на іконку "Показати пароль"
showPassword.forEach((icon, i) => {
    icon.addEventListener('click', () => {
        if (passwordInput[i].type === 'password') {
            passwordInput[i].type = 'text';
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
        } else {
            passwordInput[i].type = 'password';
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
        }
    });
});

// Обробник подій на кнопку "Підтвердити"
submitButton.addEventListener('click', event => {
    event.preventDefault(); // Зупинити перезавантаження сторінки

    const password1 = passwordInput[0].value;
    const password2 = passwordInput[1].value;
    // Перевірка паролів
    if (password1 === password2) {
        alert('You are welcome');
    } else {
        errorMessage.textContent = 'Потрібно ввести однакові значення';
        errorMessage.style.color = 'red';
        passwordInput[1].insertAdjacentElement('afterend', errorMessage);
    }
});

// Видалення повідомлення про помилку
passwordInput.forEach(input => {
    input.addEventListener('input', () => {
        if (errorMessage) {
            errorMessage.textContent = '';
        }
    });
});
